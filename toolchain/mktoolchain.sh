#!/bin/bash
IMG=${1:-lovedos_builder}
docker build --pull . -t ${IMG}
docker run --rm ${IMG} i586-pc-msdosdjgpp-gcc --version
