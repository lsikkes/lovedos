#!/bin/sh
IMG=${1:-lovedos}
docker build . -t ${IMG}
docker run --rm ${IMG} ls -lia /app/bin
#docker cp /app/bin/love.exe love

docker create -ti --name dummy ${IMG} bash
docker cp dummy:/app/bin/love.exe love.exe
docker rm -f dummy
