FROM registry.gitlab.com/lsikkes/lovedos:djcpp
RUN i586-pc-msdosdjgpp-gcc --version
COPY . /app
RUN cd /app && python ./build.py